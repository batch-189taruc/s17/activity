console.log('hello world')

/*
	1. Create a function which is able to prompt the user to provide their fullname, age, and location.
		-use prompt() and store the returned value into a function scoped variable within the function.
		-display the user's inputs in messages in the console.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
*/
	
	//first function here:
	function enterFullName(){
	let FullName = prompt('Enter your Full Name')
	console.log('Hello, ' + FullName)
	}

	enterFullName()

	let userAge = prompt('Enter your age')
	console.log('You are ' + userAge + 'years old.')

	let userLocation = prompt('Enter your location')
	console.log('You live in ' + userLocation)


/*
	2. Create a function which is able to print/display your top 5 favorite bands/musical artists.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
	
*/

	//second function here: "one", "\n", "two"

	function sayTop5Bands(){
	let topMessageBands = alert('Top 5 Favorite Bands/Musical Artists:')
	let top1Band = alert('Eraserheads')
	let top2Band = alert('The Cure')
	let top3Band = alert('Black Sabbath')
	let top4Band = alert('The Beach Boys')
	let top5Band = alert('The Smiths')
	console.log('My Top 5 Bands:')
	console.log('Eraserheads')
	console.log('The Cure')
	console.log('Black Sabbath')
	console.log('The Beach Boys')
	console.log('The Smiths')
	}

	sayTop5Bands()


/*
	3. Create a function which is able to print/display your top 5 favorite movies of all time and show Rotten Tomatoes rating.
		-Look up the Rotten Tomatoes rating of your favorite movies and display it along with the title of your favorite movie.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
	
*/
	
	//third function here:

	function sayTop5Movies(){
	let topMessageMovies = alert('Top 5 Favorite Movies/Series:')
	let top1Movie = alert('The Dark Knight Rises. Tomatometer 87% ')
	let top2Movie = alert('50 First Dates. Tomatometer 45%')
	let top3Movie = alert('Saving Private Ryan. Tomatometer 93%')
	let top4Movie = alert('The Godfather. Tomatometer 97%')
	let top5Movie = alert('Angels and Demons. Tomatometer 37%')
	console.log('My Top 5 Movies:')
	console.log('The Dark Knight Rises. Tomatometer 87% ')
	console.log('50 First Dates. Tomatometer 45%')
	console.log('Saving Private Ryan. Tomatometer 93%')
	console.log('The Godfather. Tomatometer 97%')
	console.log('Angels and Demons. Tomatometer 37%')

	}

	sayTop5Movies()

/*
	4. Debugging Practice - Debug the following codes and functions to avoid errors.
		-check the variable names
		-check the variable scope
		-check function invocation/declaration
		-comment out unusable codes.
*/


function printFriends(){
	alert("Hi! Please add the names of your friends.");
	let friend1 = prompt("Enter your first friend's name:"); 
	let friend2 = prompt("Enter your second friend's name:"); 
	let friend3 = prompt("Enter your third friend's name:");

	console.log("You are friends with:")
	console.log(friend1); 
	console.log(friend2); 
	console.log(friend3); 
};
printFriends();